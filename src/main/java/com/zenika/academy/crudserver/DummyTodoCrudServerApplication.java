package com.zenika.academy.crudserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.Random;
import java.util.UUID;

@SpringBootApplication
public class DummyTodoCrudServerApplication {

    @Autowired
    TaskRepository taskRepository;

    public static void main(String[] args) {
        SpringApplication.run(DummyTodoCrudServerApplication.class, args);
    }


    @PostConstruct
    public void initializeData() {
        taskRepository.save(new Task(new Random().nextInt(), "Créer une API REST", false, LocalDate.now().plusDays(1)));
    }
}
